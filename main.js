const toDoList = [];

const form = document.querySelector('form');
const ul = document.querySelector('ul');
const taskNumber = document.querySelector('span');
const btn = document.querySelector('button');
const input = document.querySelector('input');
const liItems = document.getElementsByClassName('task');



const addTask = (e) => {
    e.preventDefault();
    const newTask = input.value;
    console.log(newTask);
    if (newTask === "") return;
    const task = document.createElement('li');
    task.classList.add('task');
    task.innerHTML = newTask + "<button>Delete</button>";
    toDoList.push(task)
    // ul.textContent = "";
    // toDoList.forEach((toDoElement, key) => {
    //     toDoElement.dataset.key = key;
    //     ul.appendChild(toDoElement);
    // })
    renderList()
    ul.appendChild(task);
    input.value = "";
    taskNumber.textContent = liItems.length;
    console.log(liItems);
    task.querySelector('button').addEventListener('click', removeTask);
}

const removeTask = (e) => {
    e.target.parentNode.remove();
    const index = e.target.parentNode.dataset.key;
    toDoList.splice(index, 1)
    console.log(toDoList);
    taskNumber.textContent = liItems.length;
    renderList()
    // ul.textContent = "";
    // toDoList.forEach((toDoElement, key) => {
    //     toDoElement.dataset.key = key;
    //     ul.appendChild(toDoElement);
    // })
}

const renderList = () => {
    ul.textContent = "";
    toDoList.forEach((toDoElement, key) => {
        toDoElement.dataset.key = key;
        ul.appendChild(toDoElement);
    })
}


form.addEventListener('submit', addTask)



// const searchTask = (e) => {
//     const searchText = e.target.value.toLowerCase();
//     let tasks = [...liItems];
//     tasks = tasks.filter(li => li.textContent.toLowerCase().includes(searchText));
//     console.log(tasks);
//     ul.textContent = "";
//     tasks.forEach(li => ul.appendChild(li));
// }

// input.addEventListener('input', searchTask)